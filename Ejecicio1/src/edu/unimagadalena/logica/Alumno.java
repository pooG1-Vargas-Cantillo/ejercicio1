package edu.unimagadalena.logica;

public class Alumno {

	private String ID;
	private String nombre;
	private double promedio;
	public Alumno(String iD, String nombre, double promedio) {
		super();
		ID = iD;
		this.nombre = nombre;
		this.promedio = promedio;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPromedio() {
		return promedio;
	}
	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

	
	public void imprimir() {
		// TODO Auto-generated method stub
		System.out.println("==============");
		System.out.println("ID: "+ID);
		System.out.println("Nombre: "+nombre);
		System.out.println("Promedio: "+ promedio);
	}
}
