package edu.unimagadalena.logica;

import java.util.ArrayList;

public class Curso {

	private String nombre;
	private ArrayList<Alumno> alumnos;
	public Curso(String nombre)
	{
		this.nombre = nombre;
		this.alumnos = new ArrayList<Alumno>();
		
	}
	
	public void matricular(Alumno a)
	{
		if(estaInscrito(a))
		{
			System.out.println("Este alumno esta inscrito");
		}else
		alumnos.add(a);
	}
	
	public boolean remover(Alumno a)
	{
		boolean r= alumnos.remove(a);
		return r;
	}
	
	public boolean estaInscrito(Alumno a)
	{
		return alumnos.contains(a);
	}
	public void imprimirAlumnos()
	{
		for(Alumno alum: alumnos)
		{
			alum.imprimir();
			
		}
	}
	
}
