package edu.unimagadalena.app;

import edu.unimagadalena.logica.Alumno;
import edu.unimagadalena.logica.Curso;

public class Aplicacion {

	public static void main(String[] args) {
		
		Alumno a = new Alumno("1","ALFA",6.5);
		Alumno b = new Alumno("2","BETA",4.5);
		Alumno c = new Alumno("3","GAMMA",5.5);
		
		//crear curso
		Curso PEF2501 = new Curso("PEF2502");
		//atricular alfa y beta

		PEF2501.matricular(a);
		PEF2501.matricular(b);
	
		if(PEF2501.estaInscrito(b))
		{
			System.out.println("BETA esta inscrito");
		}
		else
			System.out.println("BETA no esta inscrito");	
		
		if(PEF2501.estaInscrito(c))
		{
			System.out.println("GAMMA esta inscrito");
		}
		else
			System.out.println("GAMMA no esta inscrito");	
		
		//remover beta
		if(PEF2501.estaInscrito(b))
		{
			PEF2501.remover(b);
			System.out.println("BETA sido removido");
			
		}
		else
			System.out.println("BETA sido removido");
			

	}

}
